import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import StreamCreate from "./streams/StreamCreate";
import StreamEdit from "./streams/StreamEdit";
import StreamDelete from "./streams/StreamDelete";
import StreamList from "./streams/StreamList";
import StreamShow from "./streams/StreamShow";
import Header from "./Header";

// Client Id
// 153860736070-qj5ludbkhrrss3bj4orn0jd74dfuf6a9.apps.googleusercontent.com
export const App = () => {
  return (
    <div className={"ui container"}>
      <BrowserRouter>
        <div>
          {/*Aici poti folosi o componenta sa fie vizibila mereu indif de Ruta.*/}
          <Header />
          <Route path={"/"} component={StreamList} exact={true} />
          <Route path={"/streams/new"} component={StreamCreate} exact={true} />
          <Route path={"/streams/edit"} component={StreamEdit} exact={true} />
          <Route
            path={"/streams/delete"}
            component={StreamDelete}
            exact={true}
          />
          <Route path={"/streams"} component={StreamShow} exact={true} />
        </div>
      </BrowserRouter>
    </div>
  );
};

// *** if it's not exact={true}, react uses .contains('str') fn
// so will show both links

// *** React Router prevents the browser from
//  navigating to the new page and fetching new index.html file
export default App;
