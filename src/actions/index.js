import {CREATE_STREAM, DELETE_STREAM, EDIT_STREAM, FETCH_STREAM, FETCH_STREAMS, SIGN_IN, SIGN_OUT} from "./types";
import streams from "../apis/streams";


const STREAMS_PATH = '/streams';

export const createStream = (formValues) => async dispatch => {
    const response = await streams.post(STREAMS_PATH, formValues);
    dispatch({type: CREATE_STREAM, payload: response.data})
};

export const fetchStreams = () => async dispatch => {
    const response = await streams.get(STREAMS_PATH)
    dispatch({type: FETCH_STREAMS, payload: response.data})
}

export const fetchStream = (id) => async dispatch => {
    const response = await streams.get(`${STREAMS_PATH}/${id}`);
    dispatch({type: FETCH_STREAM, payload: response.data})
}

export const deleteStream = id => async dispatch => {
    await streams.delete(`${STREAMS_PATH}/${id}`);
    dispatch({type: DELETE_STREAM, payload: id})
}

export const editStream = (id, formValues) => async dispatch => {
    const response = await streams.put(`${STREAMS_PATH}/${id}`, formValues)
    dispatch({type: EDIT_STREAM, payload: response.data})
}



export const signIn = (userId) => {
    return {
        type: SIGN_IN,
        payload: userId,
    };
};

export const signOut = () => {
    return {
        type: SIGN_OUT,
    };
};
